$(function() {
    // Инициализация CustomScroll
    if ($("div").is('.scroll-box')) {
        $(window).on("load", function () {
            $(".scroll-box").mCustomScrollbar({
                theme: "dark",
            });
        });
    }


    // Инициализация стилей для input и select
    $('input, select').styler();

    // Инициализация Fancybox
    $('[data-fancybox]').fancybox({loop : true});

    // Открытие информации
    $('.infomation-link').click(function () {
        $(this).parent().toggleClass('open-info');

        return false;
    });

    // При клике в любое место
    $("body, body *").click(function(e){
        // Сворачиваем всплавающеке меню
        if(!$(e.target).is(".menu-list, .menu-list *, .hamburger, .hamburger *")) {
            $('body').removeClass('open-menu');
        }

        // Сворачиваем информацию
        if(!$(e.target).is(".infomation-link, .infomation-link *, .infomation-box, .infomation-box *")) {
            $('.infomation').removeClass('open-info');
        }

    });
    
    // Вверх (скролл)
    $(".up-link a").click(function(){
        $("html, body").animate({scrollTop:0}, "slow");

        return false;
    });

    // При печати текста в input задает классы для перемещения label
    $('.input-search input').on('keyup', function () {
        var $this = $(this),
            val = $this.val();
        
        $(this).attr('value', val);

        if (val.length >= 1) {
            $(this).parents('.form-item-search').addClass('open-box');
            stop();
        } else {
            $(this).parents('.form-item-search').removeClass('open-box');
            stop();
        }
    });

    // При клике в любое место
    $("body, body *").click(function(e){
        // Сворачиваем информацию
        if(!$(e.target).is(".form-item-search, .form-item-search *")) {
            $('.form-item-search').removeClass('open-box');
        }
    });

    $('.search-box li').click(function () {
        $thisText = $(this).text();

        $(this).parents('.form-item-info').find('input.styler').val($thisText);
        $(this).parents('.form-item-search').removeClass('open-box');
        return false;
    });;


    if ($(window).width() >= '767') {
        // Инициализация Mosaicflow
        $('#photo-list').mosaicflow({
            minItemWidth: 250,
            minColumns: 1,
        });
    }


    // Если это мобильный
    if ($(window).width() <= '991') {
        // Новое открытие меню
        $top = '';
        $em = '';

        $(document).on('click','.hamburger-link', function(){
            if ($em < 1) {
                $top += $(window).scrollTop();
                $em = 1;
            }

            $('body').toggleClass('open-menu');

            $('body').css({
                overflow: 'hidden',
                top: -$top,
                width: '100%',
                position: 'fixed',
                left: '0px',
            });

            $('html').css('overflow', 'hidden');
            $(this).css('z-index', '11');
        });

        $(document).on('click','.open-menu .hamburger-link', function(){
            $('body').css({
                overflow: 'visible',
                top: '',
                width: '',
                position: 'relative',
                left: '',
            });
            $('html').css('overflow', 'visible');
            $(this).css('z-index', '12');
            $(window).scrollTop($top);
            $em = '';
            $top = '';
        });
        // Открытие табов на стр. Соревнования
        $(document).on('click', '.activity-title-bl', function () {
            tabId = $(this).attr('data-id-tab');
            $(this).parent().addClass('active').siblings().removeClass('active');
            $('#' + tabId).addClass('show').siblings().removeClass('show');
        });
    } else {
        // Клик по гамбургеру
        $('.hamburger-link').click(function () {
            $('body').toggleClass('open-menu');

            return false;
        });
    }
});